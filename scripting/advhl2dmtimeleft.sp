#pragma semicolon 1

#include <sourcemod>
#include <smlib>
#include <sdktools>
#include <steamtools>
#include <updater>
#include <regex>
#include <morecolors>

#define UPDATE_URL "http://bitbucket.org/gavvvr/sm-advanced-hl2dm-timeleft/raw/master/advhl2dmtimeleft.txt"

new Float:gameTimeOnLastRestart = 0.0;

new stvIndex = -1;
new minsLeft = -1;
new String:oldHN[64] = "";
new Handle:g_Cvar_Hostname = INVALID_HANDLE;

new String:gametype[16] = "";

new Handle:g_cvarOvtEnable = INVALID_HANDLE;
new Handle:g_cvarOvtExtendTime = INVALID_HANDLE;
new Handle:g_cvarOvtSound = INVALID_HANDLE;
new String:g_szOvt_Sound[PLATFORM_MAX_PATH] = "";
new Handle:g_cvarOvtMessage = INVALID_HANDLE;
new String:g_szOvt_Message[192] = "";

new Handle:g_Cvar_ShowTimer = INVALID_HANDLE;
new Handle:g_Cvar_HNTimeleft = INVALID_HANDLE;

new Handle:g_Cvar_Fraglimit = INVALID_HANDLE;
new Handle:g_Cvar_Timelimit = INVALID_HANDLE;

new Handle:g_Cvar_AllowWait = INVALID_HANDLE;

public Plugin:myinfo =
{
	name = "Advanced HL2DM timeleft",
	author = "gavvvr, hl2dm.net",
	description = "Advanced timeleft for hl2dm match servers which does not fail because of mp_restartgame command",
	version = "0.2.7",
	url = "https://hl2dm.net"
};

public OnPluginStart()
{
	if (LibraryExists("updater"))
	{
		Updater_AddPlugin(UPDATE_URL);
	}

	HookEvent("round_start", OnRoundStart);

	AddCommandListener(Command_Timeleft, "timeleft"); //overriding default timeleft command

	//Overtime-related cvars
	g_cvarOvtEnable = 		CreateConVar("sm_ovt_enable", "1.0", "Enables or disables overtime", _, true, 0.0, true, 1.0);
	g_cvarOvtExtendTime = 	CreateConVar("sm_ovt_extend", "1.0", "In minutes, how long the current map is extended", _, true, 0.0, false);
	g_cvarOvtSound = 		CreateConVar("sm_ovt_sound", "/vo/npc/male01/evenodds.wav", "Sound to play when the overtime is reached");
	g_cvarOvtMessage = 		CreateConVar("sm_ovt_message", "[SM] OVERTIME!", "Shows this message, when overtime is reached");

	g_Cvar_ShowTimer = 		CreateConVar("sm_timelefttimer", "1.0", "Defines if the ticking timer will be shown on the top of player's screen", _, true, 0.0, true, 1.0);
	g_Cvar_HNTimeleft = 	CreateConVar("sm_hostname_timeleft", "1.0", "Defines if the timeleft will be displayed in hostname", _, true, 0.0, true, 1.0);

	g_Cvar_Fraglimit = 		FindConVar("mp_fraglimit");
	g_Cvar_Timelimit = 		FindConVar("mp_timelimit");

	g_Cvar_Hostname =		FindConVar("hostname");

	g_Cvar_AllowWait = 		FindConVar("sv_allow_wait_command");
}

public Action:OnRoundStart(Handle:event, const String:name[], bool:dontBroadcast)
{
	gameTimeOnLastRestart = GetGameTime();
}

public OnMapStart()
{
	GetGameDescription(gametype, sizeof(gametype), true);
	stvIndex = GetStvIndex();
	gameTimeOnLastRestart = 0.0;
	CreateTimer(1.0, Check_TimeLeft, _, TIMER_REPEAT|TIMER_FLAG_NO_MAPCHANGE);
}

public Action:Check_TimeLeft(Handle:timer) 
{
	new Float:timeLeft = GetConVarFloat(g_Cvar_Timelimit) * 60 - GetGameTime() + gameTimeOnLastRestart;
	if (timeLeft > 0)
	{
		new mins = RoundToNearest(timeLeft) / 60;
		new secs = RoundToNearest(timeLeft) % 60;

		if (GetConVarBool(g_Cvar_ShowTimer))
		{
			SetHudTextParams(-1.0, 0.01, 1.0, 255, 255, 255, 153, 1, 0.1, 0.1, 0.1);
			for (new i = 1; i <= MaxClients; i++)
			{
				if(IsClientInGame(i) && !IsFakeClient(i))
				{
					ShowHudText(i, -1, "%d:%02d", mins, secs);
				}
			}
		}

		if (timeLeft <= 1.2 && GetConVarBool(g_cvarOvtEnable))
		{
			CreateTimer(timeLeft - 0.2, CheckScores, _, TIMER_FLAG_NO_MAPCHANGE);
		}

		if (GetConVarBool(g_Cvar_HNTimeleft) && minsLeft != -1 && mins + 1 != minsLeft)
		{
			minsLeft = mins + 1;
			decl String:hostnameWithTimeleft[64];
			Format(hostnameWithTimeleft, sizeof(hostnameWithTimeleft), "[Timeleft %dmin] %s", minsLeft, oldHN);
			// PrintToChatAll("Debug: [Timeleft %dmin] %s", minsLeft, oldHN);
			SetConVarString(g_Cvar_Hostname, hostnameWithTimeleft);
			if (stvIndex != -1)
			{
				CPrintToChat(stvIndex, "[SM] Timeleft: {aliceblue}%dmin", minsLeft);
			}
		}
	}
	return Plugin_Handled;
}

public Action:CheckScores(Handle:timer)
{
	if (StrEqual(gametype, "Team Deathmatch", false))
	{
		if (GetTeamScore(2) == GetTeamScore(3)  && Client_GetCount(true, false) != 0)
		{
			Overtime();
		}
	}
	else if (StrEqual(gametype, "Deathmatch", false)) // no teamplay
	{
		if (GetUnassignedClientCount() == 2) //duel game, 2 players on map
		{
			new client1, client2;
			for (new x = 1; x <= MaxClients; x++)
			{
				if (GetClientTeam(x) == 0)
				{
					client1 = x;
					break;
				}
			}
			for (new x = client1 + 1; x <= MaxClients; x++)
			{
				if (GetClientTeam(x) == 0)
				{
					client2 = x;
					break;
				}
			}

			if (GetClientFrags(client1) == GetClientFrags(client2))
			{
				Overtime();
			}
		}
	}
}

GetUnassignedClientCount()
{
	new playersInUnassigned;
	for(new i = 1;i < MaxClients + 1; i++)
	{
		if(IsClientInGame(i) && GetClientTeam(i)==0)
		{
			playersInUnassigned++;
		}
	}
	return playersInUnassigned;
}

Overtime()
{
	SetConVarFloat(g_Cvar_Timelimit, GetConVarFloat(g_Cvar_Timelimit) + GetConVarFloat(g_cvarOvtExtendTime), false, false);
	PlaySoundAll(g_szOvt_Sound);
	PrintToChatAll(g_szOvt_Message);
}

public OnLibraryAdded(const String:name[])
{
	if (StrEqual(name, "updater"))
	{
		Updater_AddPlugin(UPDATE_URL);
	}
}

public OnConfigsExecuted()
{
	GetConVarString(g_cvarOvtSound, g_szOvt_Sound, sizeof(g_szOvt_Sound));
	GetConVarString(g_cvarOvtMessage, g_szOvt_Message, sizeof(g_szOvt_Message));
	DisablePlugin("basetriggers");
	if (GetConVarBool(g_Cvar_HNTimeleft))
	{
		HookConVarChange(g_Cvar_AllowWait, ConVarChange_AllowWaitCmd);
	}
}

public ConVarChange_AllowWaitCmd(Handle:convar, const String:oldValue[], const String:newValue[])
{
	if (StrEqual(newValue, "0"))
	{
		new Float:timeLeft = GetConVarFloat(g_Cvar_Timelimit) * 60 - GetGameTime() + gameTimeOnLastRestart;
		if (timeLeft > 0)
		{
			GetConVarString(g_Cvar_Hostname, oldHN, sizeof(oldHN));
			new Handle:timeleftRegex = CompileRegex("\\[Timeleft \\d+min] ");
			new String:matchedString[32];
			MatchRegex(timeleftRegex, oldHN);
			GetRegexSubString(timeleftRegex, 0, matchedString, sizeof(matchedString));
			CloseHandle(timeleftRegex);
			// PrintToChatAll("Debug: strlen of matched string is: %d", strlen(matchedString));
			strcopy(oldHN, sizeof(oldHN), oldHN[strlen(matchedString)]); //cutting off previous timeleft prefix if Regex has found the substring.

			minsLeft = RoundToNearest(timeLeft) / 60;
		}
	}
}

public Action:Command_Timeleft(client, const String:command[], args)
{
	decl String:timeleftStr[1024];
	if (!GetConVarBool(g_Cvar_Timelimit))
	{
		if (!GetConVarBool(g_Cvar_Fraglimit))
		{
			timeleftStr = "[SM] No timelimit for map";
			EchoTimeleft(client, timeleftStr);
			return Plugin_Handled;
		}
		FormatEx(timeleftStr, sizeof(timeleftStr), "[SM] Map will change after a player reaches %d frags", GetConVarInt(g_Cvar_Fraglimit));
		EchoTimeleft(client, timeleftStr);
		return Plugin_Handled;
	}
	new timeLeft = RoundToNearest(GetConVarFloat(g_Cvar_Timelimit)*60 - GetGameTime() + gameTimeOnLastRestart);
	new mins, secs;
	if (timeLeft > 0)
	{
		mins = timeLeft / 60;
		secs = timeLeft % 60;
		FormatEx(timeleftStr, sizeof(timeleftStr), "[SM] Time remaining for map: %d:%02d", mins, secs);
		EchoTimeleft(client, timeleftStr);
		return Plugin_Handled;
	}
	FormatEx(timeleftStr, sizeof(timeleftStr), "[SM] Time is over");
	EchoTimeleft(client, timeleftStr);
	return Plugin_Handled;
}

EchoTimeleft(client, String:OutputStr[])
{
	if (client == 0)
	{
		ReplyToCommand(client, OutputStr);
	}
	else
	{
		PrintToChat(client, OutputStr);
	}
}

bool:DisablePlugin(const String:file[])
{
	decl String:sNewPath[PLATFORM_MAX_PATH], String:sOldPath[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, sNewPath, sizeof(sNewPath), "plugins/disabled/%s.smx", file);
	BuildPath(Path_SM, sOldPath, sizeof(sOldPath), "plugins/%s.smx", file);
	
	// If plugins/<file>.smx does not exist, ignore
	if(!FileExists(sOldPath))
			return false;

	decl String:disabledPath[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, disabledPath, sizeof(disabledPath), "plugins/disabled");
	if (!DirExists(disabledPath)) // making disabled directory in case it is missing
	{
		CreateDirectory(disabledPath, 0755);
	}
	
	// If plugins/disabled/<file>.smx exists, delete it
	if(FileExists(sNewPath))
			DeleteFile(sNewPath);
	
	// Unload plugins/<file>.smx and move it to plugins/disabled/<file>.smx
	ServerCommand("sm plugins unload %s", file);
	RenameFile(sNewPath, sOldPath);
	LogMessage("plugins/%s.smx was unloaded and moved to plugins/disabled/%s.smx", file, file);
	return true;
}

PlaySoundAll(String:sndpath[])
{
	for (new i = 1; i <= MaxClients; i++)
	{
		if(!IsClientInGame(i) || IsFakeClient(i))
		{
			continue;
		}
		ClientCommand(i, "playgamesound \"%s\"", sndpath);
	}
}

GetStvIndex()
{
	for (new i = 1; i <= MaxClients; i++)
	{
		if(IsClientConnected(i) && IsClientSourceTV(i)) //IsClientConnected required because you will get error checking for SourceTV unconnected client index
		{
			return i;
		}
	}
	return -1;
}